#!/usr/bin/python3

__unittest = True

import unittest
import xmlrunner
from subprocess import CalledProcessError, Popen, PIPE

# -----------------------------


class DictTest(unittest.TestCase):
    exe = "./program"

    def _run(self, input):
        try:
            p = Popen([self.exe], shell=False, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            stdout, stderr = p.communicate(input.encode())
            self.assertNotEqual(p.returncode, -11, "segmentation fault")
            return stdout.decode(), stderr.decode(), p.returncode
        except CalledProcessError as exc:
            print("exc")
            return

    def test_existing(self):
        """Test retrieving existing values"""
        for key, val in {
            "first word": "first word explanation",
            "second word": "second word explanation",
            "third word": "third word explanation",
        }.items():
            out, _, code = self._run(key)
            self.assertEqual(code, 0, f"key {repr(key)} exit code not 0")
            self.assertIn(val, out, f"key {repr(key)} wrong value returned")

    def test_nonexistent(self):
        out, err, code = self._run("foo")
        self.assertEqual(code, 1, f"exit code not 1")
        self.assertIn(
            "Key not found", err, f"value was returned for nonexistent key: {repr(out)}"
        )

    def test_spaces(self):
        """Test different whitespace combinations"""
        for key in ["first word", "first word\n\n\n"]:
            out, _, code = self._run(key)
            self.assertEqual(code, 0, f"key {repr(key)} exit code not 0")
            self.assertIn("first word explanation", out, f"key {repr(key)} wrong value returned")

        for key in [" first word", " first word\n", "\nfirst word", "\n   first word\n"]:
            out, err, code = self._run(key)
            self.assertEqual(code, 1, f"exit code not 1")
            self.assertIn(
                "Key not found", err, f"value was returned for nonexistent key: {repr(out)}"
            )

    def test_overrun(self):
        _, err, code = self._run("A" * 282)
        self.assertEqual(code, 255, "program did not report read error")
        self.assertIn("Error", err, "program did not report read error")


if __name__ == "__main__":
    with open("report.xml", "w") as report:
        unittest.main(
            testRunner=xmlrunner.XMLTestRunner(output=report),
            failfast=False,
            buffer=False,
            catchbreak=False,
        )
