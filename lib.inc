%ifndef LIB_INC
%define LIB_INC
extern exit
extern string_length
extern print_string
extern string_equals
extern read_char
extern read_line

%define SYS_READ	0
%define SYS_WRITE 1
%define SYS_EXIT	60

%define STDIN	0
%define STDOUT 1
%define STDERR 2

%macro print_stdout 1
	mov	rdi, STDOUT
	mov	rsi, %1
	call	print_string
%endmacro

%macro print_stderr 1
	mov	rdi, STDERR
	mov	rsi, %1
	call	print_string
%endmacro

%endif ; LIB_INC

