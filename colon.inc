%ifndef COLON_INC
%define COLON_INC

%define head 0
%macro colon 2
alignb 16
%2: 
.next:	dq head
.key:	dq ._key
.val:	dq ._val
._key:	db %1, 0
._val:
%define head %2
%endmacro

%endif ; COLON_INC
; vim:ft=nasm:sw=8:ts=8:noet
