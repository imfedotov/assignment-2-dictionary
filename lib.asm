%define SYS_READ	0
%define SYS_WRITE 1
%define SYS_EXIT	60

%define STDIN	0
%define STDOUT 1
%define STDERR 2

section .text
; Wrapper around exit() syscall
;
; noreturn exit(int code)
global exit
exit: 
	mov	rax, SYS_EXIT
	syscall

; Calculates the length of a null-terminated string
;
; size_t string_length(char *str)
global string_length
string_length:
	xor	rax, rax

	.loop:
	cmp	byte [rdi+rax], 0
	je	.end
	inc	rax
	jmp	.loop

	.end:
	ret

; Prints a null-terminated string to `fd`
;
; void print_string(int fd, char *str)
global print_string
print_string:
	push	rdi
	push	rsi
	mov	rdi, rsi
	call	string_length
	pop	rsi
	pop	rdi

	mov	rdx, rax	; len
	mov	rax, SYS_WRITE
	syscall

	ret

; Checks if two null-terminated strings are equal
;
; int string_equals(char *str1, char *str2) 
global string_equals
string_equals:
	xor	rax, rax
	xor	rcx, rcx

	.loop:
	mov	dl, [rdi+rcx]
	cmp	dl, [rsi+rcx]
	jne	.fail

	inc	rcx

	test	dl, dl
	jnz	.loop
	
	inc	rax
	.fail:
	ret

; Reads a single char from stdin. 0 if EOF.
;
; char read_char()
global read_char
read_char:
	push	0

	mov	rax, SYS_READ
	xor	rdi, rdi	; fd
	mov	rsi, rsp	; buf
	mov	rdx, 1		; len
	syscall

	cmp	rax, -1
	je	.fail
	test	rax, rax
	jz	.eof

	pop	rax
	ret
	.eof:
	add	rsp, 8
	ret
	.fail:
	xor	rax, rax
	ret

; Reads an entire line from stdin. Preserves '\n' at the end. 
; Handle return value 0 with caution. Because it's either due to EOF or buffer
; being too short, zero-termination is not guaranteed if return value is 0.
; 
; size_t read_line(char *buf, size_t sz)
global read_line
read_line:
	push	rbx
	push	r12
	push	r13

	mov	rbx, rdi
	xor	r12, r12
	mov	r13, rsi

	.loop:
	call	read_char

	cmp	r12, r13
	jae	.fail

	mov	[rbx+r12], al
	inc	r12

	test	al, al ;
	jz	.end
	cmp	al, `\n`
	jne	.loop

	cmp	r12, r13
	jae	.fail
	mov	byte [rbx+r12], 0
	inc	r12	; avoid extra logic to handle EOF length

	.end:
	lea	rax, [r12-1]
	pop	r13
	pop	r12
	pop	rbx
	ret

	.fail:
	xor	rax, rax
	pop	r13
	pop	r12
	pop	rbx
	ret

; vim:ft=nasm:sw=8:ts=8:noet
