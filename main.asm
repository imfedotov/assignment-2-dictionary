%include 'lib.inc'
%include 'dict.inc'
%include 'words.inc'

%define INBUF_LEN 0x100

section .rodata:
aPrompt: db `Word: `, 0
aNotfound: db `Key not found\n`, 0
aError: db `Error!\n`, 0

section .text
global _start
_start:
	sub	rsp, INBUF_LEN+0x8  ; alignment

	; print to stderr to allow piping
	print_stderr	aPrompt

	mov	rdi, rsp
	mov	rsi, INBUF_LEN
	call	read_line

	test	rax, rax
	jz	.iofail

	cmp	byte [rsp+rax-1], `\n`
	jne	.nonl
	mov	byte [rsp+rax-1], 0
	.nonl:

	mov	rdi, rsp
	mov	rsi, head
	call	find_word

	test	rax, rax
	jz	.notfound

	print_stdout	[rax+16]

	xor	rdi, rdi
	jmp	exit

	.notfound:
	print_stderr	aNotfound
	mov	rdi, 1
	jmp	exit

	.iofail:
	print_stderr	aError
	mov	rdi, -1
	jmp	exit


; vim:ft=nasm:sw=8:ts=8:noet
