
ASM = nasm
ASMFLAGS += -felf64

sources := $(wildcard *.asm)
headers := $(wildcard *.inc)
objects := $(patsubst %.asm,%.o,$(sources))

program: $(objects)
	$(LD) $(LDFLAGS) -o $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@

$(objects): $(headers) $(sources)

.PHONY: all
all: program 

.PHONY: test
test: program
	./test.py

.PHONY: clean
clean:
	rm *.o program
