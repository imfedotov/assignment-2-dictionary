%include 'lib.inc'

section .text

; Returns a pointer to entry with given key
;
; struct entry *find_word(char *key, entry *dict)
global find_word
find_word:
	push	rbx
	push	r12

	mov	rbx, rsi
	mov	r12, rdi

	.loop:
	test	rbx, rbx
	jz	.end

	mov	rdi, r12
	mov	rsi, [rbx+8]
	call	string_equals

	test	rax, rax
	jnz	.end

	mov	rbx, [rbx]
	jmp	.loop

	.end:
	mov	rax, rbx
	pop	r12
	pop	rbx
	ret

; vim:ft=nasm:sw=8:ts=8:noet
